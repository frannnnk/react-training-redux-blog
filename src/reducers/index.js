import { combineReducers } from 'redux';
import PostReducer from './reducer_post';
import { reducer as formReducer } from 'redux-form';


/*
import { combineReducers } from 'redux';
import WeatherReducer from './reducer_weather';

const rootReducer = combineReducers({
  //state: (state = {}) => state
   weather: WeatherReducer
});

export default rootReducer;

*/




const rootReducer = combineReducers({
  posts: PostReducer,
  form: formReducer
});

export default rootReducer;
