import _ from 'lodash';
import  { FETCH_POST , FETCH_SINGLE_POST , DELETE_POST} from '../actions';
import  {  } from '../actions';

export default function (state = {}, action) {
  switch (action.type) {
      case FETCH_POST:
       console.log ("action.payload:",action.payload) ;
        console.log ("action.payload.data:",action.payload.data) ;
        // from [post, post2] to {4: post}
        return _.mapKeys(action.payload.data, 'id');
      case FETCH_SINGLE_POST:

        // ES5
        /*
        const post = action.payload.data;
        const newState = { ...state};
        newState[post.id] = post;
        return newState;
        */

        return { ...state, [action.payload.data.id]: action.payload.data};
      case DELETE_POST:
          console.log("delete in reducer...");
          return _.omit(state, action.payload);

      default:
        return state;
  }
}
