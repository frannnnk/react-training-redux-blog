import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchSinglePost, deletePost } from '../actions';
import { Link } from 'react-router-dom';

/*

import { fetchPosts } from '../actions';
import { createPost } from '../actions';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
*/

// class component
class PostShow extends Component {

componentDidMount(){
  if (!this.props.post) {
    const {id} = this.props.match.params;
    this.props.fetchSinglePost(id);
  }

}

onDeleteClick(){
  const {id} = this.props.match.params;
  this.props.deletePost(id, () => {

    console.log("Delete callback");
    this.props.history.push('/');
  });
}


  render () {

    //const {handleSubmit}  = this.props;

    const { post } = this.props;

    if (!post) {
      return <div>Loading...</div>;
    }

    return (

      <div>
        <Link to="/">Back to index </Link>
        <h3>{post.title}</h3>
        <h6>Cat: {post.categories}</h6>
        <p>{post.content}</p>

        <button className="btn btn-danger pull-xs-right"
          onClick={this.onDeleteClick.bind(this)}
        >
        Delete
        </button>

      </div>

    );
  }

}
/*
function mapStateToProps (state) {
  // whatever is returned will show up as props
  // inside of this container

  return {
    weatherHahaha: state.weatherFrank
    // why xxx: state.weatherFrank
    // because in the index.js under recuder, we assign
    // the key "weatherFrank" to bind the reducer WeatherReducer

    // why weatherHahaha: xxx
    // because in the index.js under recuder, we assign
    // the key "weatherFrank" to bind the reducer WeatherReducer


  }
}
*/

// ES6 simplyfied version:
/*
function mapStateToProps ({ weather }) {
  // whatever is returned will show up as props
  // inside of this container
  return { weather };

  // above ES6 simplyfied version:
  // { weather } in the parameter pull out state.weather
  // which equals to state.weather
  // return { weather } == return { weather: weather }
}
*/


// Anything returned from this function will end up as props on the component
/*
function mapDispatchToProps(dispatch) {
  // Whenever selectBook is called, the result should be passed to all out reducers
  // return bindActionCreators( {fetchPosts},dispatch );
  return {};
}
*/

function mapStateToProps ( {posts}, ownProps) {
  // whatever is returned will show up as props
  // inside of this container
  return { post: posts[ownProps.match.params.id]};

  // above ES6 simplyfied version:
  // { weather } in the parameter pull out state.weather
  // which equals to state.weather
  // return { weather } == return { weather: weather }
}



// Promote BookList from a component to a container
// passing null in the first parameter means no need to manage the state
// export default connect(mapStateToProps,{ fetchPosts })(PostNew);


export default connect(mapStateToProps,{ fetchSinglePost, deletePost })(PostShow);
