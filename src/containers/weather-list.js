// Standart import
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import  Chart  from '../components/chart';
import GoogleMap from '../components/google-map'

// Action Creator
//import { fetchWeather } from '../actions/index';

// class component
class WeatherList extends Component {


  constructor(props){
    super(props);

    this.state = {term:''};

    // With below line, you don't need to write:
    // e => this.onSearchTermChange(e)
    // this is binding the context
    //this.onSearchTermChange = this.onSearchTermChange.bind(this);
    //this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  renderWeather(cityData){

    if (!cityData) {
      return "";
    }

    const temps = cityData.list.map( weather => weather.main.temp );
    const pressures = cityData.list.map( weather => weather.main.pressure );
    const humidities = cityData.list.map( weather => weather.main.humidity );



    const { lon, lat } = cityData.city.coord;

    // Samp as below:
    //const lon = cityData.city.coord.lon;
    //const lat = cityData.city.coord.lat;

    //console.log(temps);

    return (
      <tr key={cityData.city.name}>
        <td>{cityData.city.name}
          <GoogleMap lat={lat} lon={lon}/>
        </td>
        <td>
            <Chart data={temps} color="red" height={120} width={180}/>
        </td>
        <td>
          <Chart data={pressures} color="green" height={120} width={180}/>
        </td>
        <td>
          <Chart data={humidities} color="yellow" height={120} width={180}/>
        </td>
      </tr>

    );
  }

  render () {
    return (
      <table className="table table-hover">
        <thead>
          <tr >
            <th>City</th>
            <th>Temp</th>
            <th>Pressure</th>
            <th>Humidity</th>
          </tr>
        </thead>
        <tbody>

          {this.props.weatherHahaha.map( this.renderWeather )}

        </tbody>
      </table>
    );
  }



}

function mapStateToProps (state) {
  // whatever is returned will show up as props
  // inside of this container

  return {
    weatherHahaha: state.weatherFrank
    // why xxx: state.weatherFrank
    // because in the index.js under recuder, we assign
    // the key "weatherFrank" to bind the reducer WeatherReducer

    // why weatherHahaha: xxx
    // because in the index.js under recuder, we assign
    // the key "weatherFrank" to bind the reducer WeatherReducer


  }
}


// ES6 simplyfied version:
/*
function mapStateToProps ({ weather }) {
  // whatever is returned will show up as props
  // inside of this container
  return { weather };

  // above ES6 simplyfied version:
  // { weather } in the parameter pull out state.weather
  // which equals to state.weather
  // return { weather } == return { weather: weather }
}
*/


// Anything returned from this function will end up as props on the BookList container
function mapDispatchToProps(dispatch) {
  // Whenever selectBook is called, the result should be passed to all out reducers
  // return bindActionCreators( {fetchWeather},dispatch );
  return {};
}


// Promote BookList from a component to a container
// passing null in the first parameter means no need to manage the state
export default connect(mapStateToProps,mapDispatchToProps)(WeatherList);
