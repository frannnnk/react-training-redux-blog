import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPosts } from '../actions';
import { Link } from 'react-router-dom';


// class component
class PostIndex extends Component {

  componentDidMount(){
    this.props.fetchPosts();
  }

  renderPosts(){
    return _.map(this.props.posts, post => {

        return (
            <li className="list-group-item" key={post.id}>
              <Link to={`/posts/${post.id}`}>{post.title}</Link>
            </li>
        )
    });
  }


  render () {
    console.log("posts:", this.props.posts );
    return (

      <div>
        <div className="text-xs-right">
          <Link className="btn btn-success" to="/posts/new">
            Add a Post
          </Link>
        </div>
        <h3>Post Index Page</h3>
        <ul className="list-group">
          {this.renderPosts()}
        </ul>
      </div>

    );
  }

}
/*
function mapStateToProps (state) {
  // whatever is returned will show up as props
  // inside of this container

  return {
    weatherHahaha: state.weatherFrank
    // why xxx: state.weatherFrank
    // because in the index.js under recuder, we assign
    // the key "weatherFrank" to bind the reducer WeatherReducer

    // why weatherHahaha: xxx
    // because in the index.js under recuder, we assign
    // the key "weatherFrank" to bind the reducer WeatherReducer


  }
}
*/

// ES6 simplyfied version:
/*
function mapStateToProps ({ weather }) {
  // whatever is returned will show up as props
  // inside of this container
  return { weather };

  // above ES6 simplyfied version:
  // { weather } in the parameter pull out state.weather
  // which equals to state.weather
  // return { weather } == return { weather: weather }
}
*/


// Anything returned from this function will end up as props on the component
/*
function mapDispatchToProps(dispatch) {
  // Whenever selectBook is called, the result should be passed to all out reducers
  // return bindActionCreators( {fetchPosts},dispatch );
  return {};
}
*/

function mapStateToProps (state) {
  // whatever is returned will show up as props
  // inside of this container
  return { posts: state.posts };

  // above ES6 simplyfied version:
  // { weather } in the parameter pull out state.weather
  // which equals to state.weather
  // return { weather } == return { weather: weather }
}



// Promote BookList from a component to a container
// passing null in the first parameter means no need to manage the state
export default connect(mapStateToProps,{ fetchPosts })(PostIndex);
