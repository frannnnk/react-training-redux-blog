import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPosts } from '../actions';
import { createPost } from '../actions';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';



// class component
class PostNew extends Component {

componentDidMount(){
    //this.props.fetchPosts();
}


 renderField (field) {

   const className = `form-group ${ field.meta.touched && field.meta.error ? 'has-danger' : ''}`;

   return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className="form-control"
          type="text"
          {...field.input}
        />
        <span className="text-help">{field.meta.touched ? field.meta.error : ''}</span>
      </div>
   )
 }


onSubmitMyLovelyForm(values){
  console.log(values);
  this.props.createPost(values, () => {
      this.props.history.push('/');
  });



  // call action creater


}


  render () {

    //const {handleSubmit}  = this.props;

    return (

      <div>
        <h3>Post New</h3>

        <form onSubmit={ this.props.handleSubmit(this.onSubmitMyLovelyForm.bind(this)) }>
          <Field
              name="title"
              label="Title"
              component={ this.renderField }
          />

          <Field
              name="categories"
              label="Categories"
              component={ this.renderField }
          />

          <Field
              name="content"
              label="Post Content"
              component={ this.renderField }
          />

          <button type="submit" className="btn btn-success">Submit</button>
          <Link to="/" className="btn btn-danger">Back</Link>

        </form>

      </div>

    );
  }

}
/*
function mapStateToProps (state) {
  // whatever is returned will show up as props
  // inside of this container

  return {
    weatherHahaha: state.weatherFrank
    // why xxx: state.weatherFrank
    // because in the index.js under recuder, we assign
    // the key "weatherFrank" to bind the reducer WeatherReducer

    // why weatherHahaha: xxx
    // because in the index.js under recuder, we assign
    // the key "weatherFrank" to bind the reducer WeatherReducer


  }
}
*/

// ES6 simplyfied version:
/*
function mapStateToProps ({ weather }) {
  // whatever is returned will show up as props
  // inside of this container
  return { weather };

  // above ES6 simplyfied version:
  // { weather } in the parameter pull out state.weather
  // which equals to state.weather
  // return { weather } == return { weather: weather }
}
*/


// Anything returned from this function will end up as props on the component
/*
function mapDispatchToProps(dispatch) {
  // Whenever selectBook is called, the result should be passed to all out reducers
  // return bindActionCreators( {fetchPosts},dispatch );
  return {};
}
*/

function mapStateToProps (state) {
  // whatever is returned will show up as props
  // inside of this container
  return { posts: state.posts };

  // above ES6 simplyfied version:
  // { weather } in the parameter pull out state.weather
  // which equals to state.weather
  // return { weather } == return { weather: weather }
}


function validateMyFromPlease(values){
  //console.log(values);
  const errors = {} ;

  if (!values.title) {
    errors.title = "Enter a title";
  }
  if (!values.categories) {
    errors.categories = "Enter a categories";
  }
  if (!values.content) {
    errors.content = "Enter a content";
  }

  // if errors is empty, the form is fine to submit
  // if errors has eny properties, the form is invalid.
  return errors;
}


// Promote BookList from a component to a container
// passing null in the first parameter means no need to manage the state
// export default connect(mapStateToProps,{ fetchPosts })(PostNew);

export default reduxForm({
  validate: validateMyFromPlease,
  form: 'PostsNewForm' // this string need to be unique
})(
  connect( null, { createPost })(PostNew)
);
