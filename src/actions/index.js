import axios from 'axios';

export const FETCH_POST = 'fetch_posts';
export const CREATE_POST = 'create_posts';
export const DELETE_POST = 'delete_posts';
export const FETCH_SINGLE_POST = 'fetch_single_post';
export const API_KEY = 'FRANKSsdw90';
export const ROOT_URL = 'http://reduxblog.herokuapp.com/api';



export function fetchPosts() {

  const url = `${ROOT_URL}/posts?key=${API_KEY}`;
  const request = axios.get(url);


  return {
    type: FETCH_POST,
    payload: request
  }
}

export function createPost(values, callback) {

  const url = `${ROOT_URL}/posts?key=${API_KEY}`;
  const request = axios.post( url, values ).then( () => callback());


  return {
    type: CREATE_POST,
    payload: request
  }
}

export function fetchSinglePost(id) {
  console.log("getting "+id);
  const url = `${ROOT_URL}/posts/${id}?key=${API_KEY}`;
  const request = axios.get( url );


  return {
    type: FETCH_SINGLE_POST,
    payload: request
  }
}

export function deletePost(id, callback) {
  console.log("deleteing "+id);
  const url = `${ROOT_URL}/posts/${id}?key=${API_KEY}`;
  const request = axios.delete(url)
    .then( () => callback() );


  return {
    type: DELETE_POST,
    payload: id
  }
}




/*
export function selectBook(book) {
  console.log('A book has been selected', book.title);
  // selectBook is an action creator, it need to return an actions
  // , an object with a type property
  return {
    type: 'BOOK_SELECTED',
    payload: book
  }
}
*/
